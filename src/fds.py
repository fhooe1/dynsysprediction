import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from functions import grammar_u as grm
import re

from scipy.integrate import solve_ivp

import pandas as pd


def replacenth(string, sub, wanted, n):
    where = [m.start() for m in re.finditer(sub, string)][n]
    before = string[:where]
    after = string[where:]
    after = after.replace(sub, wanted, 1)
    return before + after


data = pd.read_csv('part.csv', index_col='index')
data = data.iloc[:, 1:]

# create data to be fitted
x = np.arange(0, len(data)-1)
y = data['c0_y']
u = data.drop(['c0_y'], axis=1).to_numpy()[:-1].T

u0, u1, u2, u3, u4, u5, u6, u7, u8, u9, u10 = u

y_diff = y.diff().dropna().to_numpy()
funs = grm.load_functions(max_complexity=3, var_count=1, uval_count=11)


def fun_eval(t, y0, u, p, fun):
    return eval(fun)


for fun in funs:
    plt.plot(list(data.index[:-1]), y.iloc[:-1], 'b-', label='data')
    class fit_class:
        def __init__(self, fun):
            self.fun = fun

        def func(self, xi, p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10):
            x0, u0, u1, u2, u3, u4, u5, u6, u7, u8, u9, u10 = xi
            return eval(self.fun)

    for px in range(fun.count('p')):
        fun = replacenth(fun, 'p', f"p{px}", px)
    fun = fun.replace('[', '').replace(']', '')

    inst = fit_class(fun)
    xi = (x, u0, u1, u2, u3, u4, u5, u6, u7, u8, u9, u10)

    params, _ = curve_fit(inst.func, xi, y_diff)


    y0 = [y.iloc[0]]

    for ui in reversed(range(11)):
        fun = fun.replace(f'u{ui}', f'u[{ui}][int(t)]')
    for pi in range(fun.count('p')):
        fun = fun.replace(f'p{pi}', f'p[{pi}]')

    fun = fun.replace('x0', 'y0')
    sol = solve_ivp(fun_eval, [0, len(y)-2], y0, args=(u, params, fun), t_eval=range(0, len(y)-2))
    plt.plot(list(data.index[:-2]), sol.y.T, 'r-', label='predicted')
    plt.show()

print(1)
