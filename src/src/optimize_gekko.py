import matplotlib.pyplot as plt
import pandas as pd
from gekko import GEKKO
from sklearn.metrics import mean_squared_error

df = pd.read_csv('exp.csv', sep=";", decimal=',')
t = df['t'].to_numpy()
ym = df['y'].to_numpy()

with open('functions_complexity_3.txt', 'r') as f:
    functions = f.read().splitlines()

for fun in functions:
    print(fun)
    m = GEKKO()
    m.options.SOLVER = 0

    x = m.Param(value=t)

    p0 = m.FV()
    p0.STATUS = 1
    p1 = m.FV()
    p1.STATUS = 1
    p2 = m.FV()
    p2.STATUS = 1
    p3 = m.FV()
    p3.STATUS = 1
    p4 = m.FV()
    p4.STATUS = 1
    p5 = m.FV()
    p5.STATUS = 1
    p6 = m.FV()
    p6.STATUS = 1

    y = m.CV(value=ym)  # controlled variable
    y.FSTATUS = 1

    try:
        m.Equation(y==eval(fun))
        m.options.IMODE = 4

        m.options.COLDSTART = 2
        m.solve(disp=False)
        # m.solve(disp=True)

        m.options.TIME_SHIFT = 0
        m.options.COLDSTART = 0
        m.solve(disp=False)

        print(round(mean_squared_error(ym, y.value, squared=False), 2))
        print(f'p1={p1}, p1={p2}, p1={p3}, p1={p4}, p1={p5}, p1={p6}')

        plt.plot(t, ym, 'bo', label='Data')
        plt.plot(x.value, y.value, 'r-', label='Regression')
        plt.legend()
        plt.show()
    except:
        print('FAILED!')

    m.cleanup()