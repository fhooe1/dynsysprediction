import json
import math

with open('out\out.txt', 'r') as f:
	lines = f.readlines()

quality = {}
for f in lines:
	if f[:3] == 'run':
		o1 = f
	if f[:3] == 'r_s':
		quality[o1[:-2]] = float(f.split(" ")[-1][:-1])

print(json.dumps(quality, indent=4))

x = list(quality.values())
x.sort(reverse=True)
best = max([ix for ix in x if not math.isnan(ix)])
print(best)
print(list(quality.keys())[list(quality.values()).index(best)])

