import symfit.core.models
from symfit import variables, parameters, ODEModel, D, Fit
import re
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

import warnings
warnings.filterwarnings("ignore")


def replace_p(functions):
    pi = 0
    np = 0
    for ix, f in enumerate(functions):
        np += len(re.findall('(?<!x)p', f))
        for i in range(pi, np):
            f = re.sub('(?<!x)p(?!\d)', f'p{i}', f, count=1)
        pi += np
        functions[ix] = f
    return functions


df = pd.read_csv('lotka_volterra_2.csv', sep=";", decimal=',').iloc[:250]

my_input = 'functions_complexity_3.txt'
with open(my_input, 'r') as f:
    functions = f.read().splitlines()

for dxdti, dxdt in enumerate(functions):
    for dydti, dydt in enumerate(functions):
        try:
            dxdt, dydt = replace_p([dxdt, dydt])

            t, x1, x2 = variables('t, x1, x2')
            p0, p1, p2, p3, p4, p5, p6, p7, p8 = parameters('p0, p1, p2, p3, p4, p5, p6, p7, p8')

            tdata = df['t'].to_numpy()
            data = {x1: df['predator'].to_numpy(),
                    x2: df['prey'].to_numpy()}

            # dxdt = "p0*x1 - p1*x1*x2"
            # dydt = "p2*x1*x2 - p3*x2"

            model_dict = {
                D(x1, t): eval(dxdt),
                D(x2, t): eval(dydt),

            }
            model = ODEModel(
                model_dict,
                initial={t: 0.0, x1: data[x1][0], x2: data[x2][0]}
            )

            print(f'run_{dxdti+1}_{dydti+1}')
            print(model)

            # tdata = np.linspace(0, 20, 100)
            # data = model(t=tdata, p0=0.1, p1=0.2, p2=0.3, p3=0.3)._asdict()
            # sigma_data = 0.3
            # np.random.seed(42)
            # for var in data:
            #     data[var] += np.random.normal(0, sigma_data, size=len(tdata))

            # k1_f.min, k1_f.max = 0, 1
            # k1_r.min, k1_r.max = 0, 1
            # k2_f.min, k2_f.max = 0, 1
            # k2_r.min, k2_r.max = 0, 1

            fit = Fit(model, t=tdata, x1=data[x1], x2=data[x2])
            fit_result = fit.execute()
            print(fit_result)

            taxis = np.linspace(tdata.min(), tdata.max(), 1000)
            model_fit = model(t=taxis, **fit_result.params)._asdict()
            for var in data:
                plt.scatter(tdata, data[var], label='{}_measured'.format(var.name))
                plt.plot(taxis, model_fit[var], label='{}_predicted'.format(var.name))
            plt.legend()
            plt.title(list(model.model_dict.values()))
            plt.savefig(f'out/run_{dxdti+1}_{dydti+1}.png')
            plt.close()
            with open('out\out.txt', 'a') as f:
                f.write(f'run_{dxdti+1}_{dydti+1}:\n{model}\n{fit_result}\n\n\n')
        except symfit.core.models.ODEError:
            print('ERROR!')
        print('\n')