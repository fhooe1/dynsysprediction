import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

from symfit import Parameter, Variable, Fit, exp

with open('functions_complexity_3.txt', 'r') as f:
    functions = f.read().splitlines()

df = pd.read_csv('exp.csv', sep=";", decimal=',')
t = df['t'].to_numpy()
ym = df['y'].to_numpy()

for fun in functions:
    p0 = Parameter('p0')
    p1 = Parameter('p1')
    p2 = Parameter('p2')
    p3 = Parameter('p3')
    p4 = Parameter('p4')
    p5 = Parameter('p5')
    x = Variable('x')
    model = eval(fun)
    
    fit = Fit(model, t, ym)
    try:
        fit_result = fit.execute()

        y = model(x=t, **fit_result.params)
        plt.plot(t, y, label='predicted')
        plt.plot(t, ym, 'x', label='measured', markersize=4)
        plt.title(fun)
        plt.legend()
        plt.show()
    except:
        print('ERROR')
