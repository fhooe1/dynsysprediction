from algorithms import lmft, gkk, scp_ptmz_merge, scp_ptmz
from problems import lotka_volterra, sir, exp, industrial_robot
from functions import grammar_u as grm
import itertools
from joblib import Parallel, delayed
from multiprocessing import cpu_count


def main():
    train, validation, test = industrial_robot.load_problem_data(0.8)
    functions = grm.load_functions(max_complexity=3, var_count=1, uval_count=11)

    targets = [c for c in train.columns if '_y' in c]

    fun_use = 'curve_fit'

    with open(f'../docs/results_{fun_use}.csv', 'w') as f:
        f.write(f'id;rmse;corr;train/test;fun\n')

    for i, t in enumerate(targets):
        print(f'calculate for target: {t}')

        possible_combinations = list(itertools.product(*[functions]))

        cores_use = cpu_count() - 1
        cores_use = 1
        results = Parallel(n_jobs=cores_use)(delayed(scp_ptmz_merge.run)(i_combination, combination, train, validation,
                                                                   'index', t,
                                                                   train.drop(['index', t], axis=1).columns, i)
                                             for i_combination, combination in enumerate(possible_combinations))

        # for i_combination, combination in enumerate(possible_combinations):
        #     scp_ptmz.run(i_combination, combination, train, validation, 'index', t, train.drop(['index', t], axis=1).columns, i)


if __name__ == '__main__':
    main()

