import numpy as np
import matplotlib.pyplot as plt
from gekko import GEKKO

m = GEKKO()

nt = 101
m.time = np.linspace(0, 1, nt)

r = 0.9
a = 1.0
f = [a * r ** i for i in range(nt)]

# Parameters
u = m.MV()
u.STATUS = 1

# Variables
x1 = m.Var()

p = np.zeros(nt)
p[-1] = 1.0
final = m.Param(value=p)

# Equations
m.Equation(x1.dt() == -(u + 0.5 * u ** 2) * x1)

# Objective Function
m.Minimize((x1-f)**2 * final)

m.options.IMODE = 6
m.solve()


plt.figure(1)
plt.plot(m.time, x1.value, 'k:', lw=2, label=r'$x_1$')
plt.plot(m.time, u.value, 'r--', lw=2, label=r'$u$')
plt.legend(loc='best')
plt.xlabel('Time')
plt.ylabel('Value')
plt.show()
