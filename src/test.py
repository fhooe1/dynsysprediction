from scipy.integrate import solve_ivp
from problems import industrial_robot
import numpy as np
import matplotlib.pyplot as plt

from sklearn.metrics import mean_squared_error

def f(t, y0, u):
    return 0.36345*u[0][int(t)] - 4e-05*u[7][int(t)]*u[9][int(t)] + 0.0246


train, validation, test = industrial_robot.load_problem_data(0.8)
targets = [c for c in test.columns if '_y' in c]

t = targets[0]

segments = np.array_split(test, 10)

for i, s in enumerate(segments):
    xm = s['index'].to_numpy()[:-1]
    um = s[train.drop(['index', t], axis=1).columns].to_numpy()[:-1].T

    y = np.diff(s[t].to_numpy())
    y_real = s[t].to_numpy()
    y0 = [y_real[0]]

    sol = solve_ivp(f, [0, len(y)-2], y0, args=(um, ), t_eval=range(0, len(y)-2))

    print(f'segment {i}:')
    rmse = mean_squared_error(sol.y.T, y_real[:-3], squared=False)
    corr = np.corrcoef(sol.y[0], y_real[:-3])[0][1]
    print(f'rmse: {rmse}')
    print(f'correlation: {corr}')
    print()

    plt.plot(y_real, label='original')
    plt.plot(sol.y.T, label='predicted')
    plt.legend()
    plt.title(f'segment {i}')
    plt.show()

xm = test['index'].to_numpy()[:-1]
um = test[train.drop(['index', t], axis=1).columns].to_numpy()[:-1].T

y = np.diff(test[t].to_numpy())
y_real = test[t].to_numpy()
y0 = [y_real[0]]

sol = solve_ivp(f, [0, len(y)-2], y0, args=(um, ), t_eval=range(0, len(y)-2))

print(f'total:')
rmse = mean_squared_error(sol.y.T, y_real[:-3], squared=False)
corr = np.corrcoef(sol.y[0], y_real[:-3])[0][1]
print(f'rmse: {rmse}')
print(f'correlation: {corr}')

plt.plot(y_real, label='original')
plt.plot(sol.y.T, label='predicted')
plt.legend()
plt.title(f'total')
plt.show()