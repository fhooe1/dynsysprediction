import re
from sympy import symbols, Symbol
from sympy.parsing.sympy_parser import parse_expr
import matplotlib.pyplot as plt
import time


def load_functions(max_complexity, var_count):
    with open(f'../data/functions/functions_complexity_{max_complexity}_variables_{var_count}.txt', 'r') as f:
        functions = f.read().splitlines()
    return functions


def create_functions(max_complexity, var_count):
    def replacenth(string, sub, wanted, n):
        where = [m.start() for m in re.finditer(sub, string)][n]
        before = string[:where]
        after = string[where:]
        after = after.replace(sub, wanted, 1)
        return before + after

    grammar = {
        'Expr': ['Term + Expr', 'Term + p', 'Term - Expr', 'Term - p'],
        'Term': ['Factor * Term', 'Factor * p'],
        'Factor': ['var', 'NLF'],  # Nonlinear Factor
        'NLF': [],  # Nonlinear Function
        'FAE': ['FAT + FAE', 'FAT + p'],  # FuncArgExpr
        'FAT': ['var * FAE', 'var + p'],  # FuncArgTerm
        'var': [f'x{i}' for i in range(var_count)]
    }

    increase_complexity_variables = grammar['var'] + list(grammar.keys())

    variable_renaming = {
        'x1': 'x1'
    }

    def calculate_complexity(element, increase_complexity):
        return sum([element.count(op) for op in increase_complexity])

    def calculate_grammar(complexity):
        start = time.time()
        queue = ['Expr']
        final_queue = []
        queue_evaluated = []

        length = []

        while queue:
            length.append([len(queue), len(final_queue)])
            print(length[-1])
            element = queue.pop()
            if element in queue_evaluated:
                continue
            queue_evaluated.append(element)
            for k in grammar.keys():
                for f in range(element.count(k)):
                    for o in grammar[k]:
                        new_element = replacenth(element, k, o, f)
                        if (not any(ext in new_element for ext in grammar.keys())) and \
                                (calculate_complexity(new_element, increase_complexity_variables) <= complexity):
                            new_element_parsed = Symbol(re.sub('(?<!x)\d\*', '', str(parse_expr(new_element))))
                            if new_element_parsed not in final_queue:
                                final_queue.append(new_element_parsed)
                            continue
                        if calculate_complexity(new_element, increase_complexity_variables) <= complexity:
                            queue.append(new_element)
                            continue
        duration = time.time() - start
        print(f'complexity {complexity}: length {len(final_queue)}, duration: {round(duration, 2)}')
        return final_queue

    functions = calculate_grammar(max_complexity)

    """
    functions = []
    for f in functions_raw:
        g = str(f)
        for i in range(g.count('p')):
            g = replacenth(g, 'p', f'p[{i}]', i)
        functions.append(g)
    """

    with open(f'../data/functions/functions_complexity_{max_complexity}_variables_{var_count}.txt', 'w') as f:
        for e in functions:
            je = str(e)
            rep = dict((re.escape(k), v) for k, v in variable_renaming.items())
            pattern = re.compile("|".join(rep.keys()))
            je = pattern.sub(lambda m: rep[re.escape(m.group(0))], je)
            f.write(f"{str(je)}\n")
    print(f'length of final queue: {len(functions)}')

    return [str(f) for f in functions]

