import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from functions import grammar_u as grm
import re
import itertools
from sklearn.metrics import mean_squared_error

from scipy.integrate import solve_ivp

import pandas as pd

def replacenth(string, sub, wanted, n):
    where = [m.start() for m in re.finditer(sub, string)][n]
    before = string[:where]
    after = string[where:]
    after = after.replace(sub, wanted, 1)
    return before + after


def run(i_combination, combination, problem_train, problem_validation, x_name, y_name, u_name, ti):
    import warnings
    warnings.filterwarnings("ignore")

    combination = list(combination)

    rmse = []
    correlation = []
    parameter = []

    def fun_eval(t, y0, u, p, fun):
        return eval(fun)

    for i_training_segment in range(10):
        start_train = i_training_segment * 3200 + 1000
        end_train = start_train + 400
        # print(f'training segments: {start_train}-{end_train}')
        problem_use = problem_train.iloc[start_train:end_train]

        xm = problem_use[x_name].to_numpy()[:-1]
        ym = np.diff(problem_use[y_name].to_numpy())
        um = problem_use[u_name].to_numpy()[:-1].T

        y_real = problem_use[y_name].iloc[:-2]

        fun = combination[0]

        try:
            u0, u1, u2, u3, u4, u5, u6, u7, u8, u9, u10 = um
            class fit_class:
                def __init__(self, fun):
                    self.fun = fun

                def func(self, xi, p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10):
                    x0, u0, u1, u2, u3, u4, u5, u6, u7, u8, u9, u10 = xi
                    return eval(self.fun)

            for px in range(fun.count('p')):
                fun = replacenth(fun, 'p', f"p{px}", px)
            fun = fun.replace('[', '').replace(']', '')

            inst = fit_class(fun)
            xi = (xm, u0, u1, u2, u3, u4, u5, u6, u7, u8, u9, u10)

            params, _ = curve_fit(inst.func, xi, ym)

            for px in range(11):
                fun = fun.replace(f'u[{px}]', f'u[{px}][int(t)]')

            def fun_eval(t, y0, u, p, fun):
                return eval(fun)

            y0 = [y_real.iloc[0]]

            for ui in reversed(range(11)):
                fun = fun.replace(f'u{ui}', f'u[{ui}][int(t)]')
            for pi in range(fun.count('p')):
                fun = fun.replace(f'p{pi}', f'p[{pi}]')

            fun = fun.replace('x0', 'y0')
            sol = solve_ivp(fun_eval, [0, len(ym)-2], y0, args=(um, params, fun), t_eval=range(0, len(ym)-2))

            this_rmse = round(mean_squared_error(y_real[:-1], sol.y[0], squared=False), 5)
            this_correlation = round(np.corrcoef(y_real[:-1], sol.y[0])[0][1], 5)

            rmse.append(this_rmse)
            correlation.append(this_correlation)

            parameter.append([round(pi, 5) for pi in params if pi != 1.])

            plt.figure(figsize=[15, 10])
            plt.plot(list(y_real), 'bo', label='Data')
            plt.plot(sol.y[0], 'g-', label='Regression solve_ivp')
            plt.legend()
            plt.title(combination)
            plt.figtext(0.7, 0.2, f'RMSE: {this_rmse} ', wrap=True, horizontalalignment='left', fontsize=12)
            plt.figtext(0.7, 0.17, f'Correlation: {this_correlation} ', wrap=True, horizontalalignment='left',
                        fontsize=12)
            plt.figtext(0.7, 0.14, f'Parameter: {", ".join([f"p{i}={pi}" for i, pi in enumerate([round(pi, 5) for pi in params if pi != 1.])])}',
                        wrap=True, horizontalalignment='left', fontsize=12)
            plt.tight_layout()
            plt.savefig(f'../docs/plots/curve_fit/{ti}_{i_combination}_{i_training_segment}.png')
            plt.close()

        except:
            print('FAILED!')
            print()

    if len(parameter) > 0:
        with open(f'../docs/results_curve_fit.csv', 'a') as f:
            f.write(f'{ti}_{i_combination};{round(np.mean(rmse), 5)};'
                    f'{round(np.mean(correlation), 5)};training;{combination[0]}\n')

        p_param = np.mean(np.array([np.array(pi) for pi in parameter]), axis=0)

        rmse = []
        correlation = []

        for si in range(fun.count('s')):
            fun = fun.replace(f"'s{si}'", str(si))

        for index_j in range(5):
            idx_start_val = index_j * 1600 + 500
            idx_end_val = idx_start_val + 400
            problem_use_val = problem_validation.iloc[idx_start_val:idx_end_val]
            y_real = list(problem_use_val[y_name].iloc[:-1])
            y0 = [problem_use_val[y_name].iloc[0]]
            u = problem_use_val[u_name].to_numpy().T
            try:
                sol = solve_ivp(fun_eval, [0, len(problem_use_val) - 1], y0, args=(u, p_param, fun), t_eval=range(0, len(problem_use_val) - 1))
                this_rmse = round(mean_squared_error(y_real, sol.y[0], squared=False), 5)
                this_correlation = round(np.corrcoef(y_real, sol.y[0])[0][1], 5)

                rmse.append(this_rmse)
                correlation.append(this_correlation)

                plt.figure(figsize=[15, 10])
                plt.plot(y_real, 'bo', label='Data')
                plt.plot(sol.y[0], 'g-', label='Regression solve_ivp')
                plt.legend()
                plt.title(combination)
                plt.figtext(0.7, 0.2, f'RMSE: {this_rmse} ', wrap=True, horizontalalignment='left', fontsize=12)
                plt.figtext(0.7, 0.17, f'Correlation: {this_correlation} ', wrap=True, horizontalalignment='left',
                            fontsize=12)
                plt.figtext(0.7, 0.14,
                            f'Parameter: {", ".join([f"p{i}={round(pi, 5)}" for i, pi in enumerate(p_param)])}',
                            wrap=True, horizontalalignment='left', fontsize=12)
                plt.tight_layout()
                plt.savefig(f'../docs/plots/curve_fit/validation/{ti}_{i_combination}_{index_j}.png')
                plt.close()
            except:
                print('Validation Error')

        if len(correlation) > 0:
            print(f'{i_combination} - used function: {combination[0]}')
            print('VALIDATION:')
            print('Parameter', p_param)
            print('RMSE: ', round(np.mean(rmse), 5))
            print('Correlation', round(np.mean(correlation), 5))
            print()

            with open(f'../docs/results_curve_fit.csv', 'a') as f:
                f.write(f'{ti}_{i_combination};{round(np.mean(rmse), 5)};'
                        f'{round(np.mean(correlation), 5)};test;{combination[0]}\n')
