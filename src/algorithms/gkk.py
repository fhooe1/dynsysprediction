import matplotlib.pyplot as plt
from gekko import GEKKO
from sklearn.metrics import mean_squared_error
import itertools
import re
import numpy as np
from scipy.integrate import solve_ivp


def replacenth(string, sub, wanted, n):
    where = [m.start() for m in re.finditer(sub, string)][n]
    before = string[:where]
    after = string[where:]
    after = after.replace(sub, wanted, 1)
    return before + after


def run(functions, problem_train, problem_validation, problem_test, x_name, y_name, u_name, ti):
    with open(f'../docs/results_gekko.csv', 'w') as f:
        f.write(f'id;rmse;corr;train/test;fun\n')
    possible_combinations = list(itertools.product(*[functions]))

    for i_combination, combination in enumerate(possible_combinations):
        print(f'{i_combination} - used function: {combination[0]}')
        combination = list(combination)
        pi = 0
        for ix, ci in enumerate(combination):
            for px in range(ci.count('p')):
                ci = replacenth(ci, 'p', f'p[{pi}]', px)
                pi += 1
            combination[ix] = ci

        rmse = []
        correlation = []
        parameter = []

        def fun_eval(t, y0, u, p, fun):
            return eval(fun)

        for i_training_segment in range(10):
            start_train = i_training_segment * 3200 + 1000
            end_train = start_train + 400
            # print(f'training segments: {start_train}-{end_train}')
            problem_use = problem_train.iloc[start_train:end_train]

            xm = problem_use[x_name].to_numpy()[:-1]
            ym = np.diff(problem_use[y_name].to_numpy())
            um = problem_use[u_name].to_numpy()[:-1]

            m = GEKKO(remote=False)
            # m.options.SOLVER = 0

            x0 = m.Param(value=xm)

            p = [m.FV() for _ in range(pi)]
            for px in p:
                px.STATUS = 1

            u = [m.Param(value=um[:, ui]) for ui in range(um.shape[1])]

            z = m.Param(value=ym)
            y = m.Var(value=ym[0])

            try:
                m.Equation(y == eval(combination[0]))
                m.Minimize((y-z)**2)

                m.options.IMODE = 2

                m.options.COLDSTART = 2
                m.solve(disp=False)

                m.options.SOLVER = 1
                m.options.TIME_SHIFT = 0
                m.options.COLDSTART = 0
                m.solve(disp=False)

                p_param = [pi[0] for pi in p]
                myfun = combination[0]
                for uiii in range(len(u)):
                    myfun = myfun.replace(f'u[{uiii}]', f'u[{uiii}][int(t)]')
                myfun = myfun.replace('x0', 'y0')
                y_real = problem_use[y_name].iloc[:-2]
                y0 = [y_real.iloc[0]]
                sol = solve_ivp(fun_eval, [0, len(z)-1], y0, args=(u, p_param, myfun), t_eval=range(0, len(z)-1))

                this_rmse = round(mean_squared_error(y_real, sol.y[0], squared=False), 5)
                this_correlation = round(np.corrcoef(y_real, sol.y[0])[0][1], 5)

                rmse.append(this_rmse)
                correlation.append(this_correlation)

                parameter.append([round(pi.VALUE[0], 5) for i, pi in enumerate(p)])

                plt.figure(figsize=[15, 10])
                plt.plot(list(y_real), 'bo', label='Data')
                plt.plot(sol.y[0], 'g-', label='Regression solve_ivp')
                plt.legend()
                plt.title(combination)
                plt.figtext(0.7, 0.2, f'RMSE: {this_rmse} ', wrap=True, horizontalalignment='left', fontsize=12)
                plt.figtext(0.7, 0.17, f'Correlation: {this_correlation} ', wrap=True, horizontalalignment='left',
                            fontsize=12)
                plt.figtext(0.7, 0.14, f'Parameter: {", ".join([f"p{i}={round(pi.VALUE[0], 5)}" for i, pi in enumerate(p)])}',
                            wrap=True, horizontalalignment='left', fontsize=12)
                plt.tight_layout()
                plt.savefig(f'../docs/plots/gekko/{ti}_{i_combination}_{i_training_segment}.png')
                plt.close()

            except:
                print('FAILED!')
                print()

            m.cleanup()

        if len(parameter) > 0:
            print('RMSE: ', round(np.mean(rmse), 5), '; ', round(np.median(rmse), 5))
            print('Correlation', round(np.mean(correlation), 5), '; ', round(np.median(correlation), 5))
            print('Parameter', parameter)

            with open(f'../docs/results_gekko.csv', 'a') as f:
                f.write(f'{ti}_{i_combination};{round(np.mean(rmse), 5)};'
                        f'{round(np.mean(correlation), 5)};training;{combination[0]}\n')

            p_param = np.mean(np.array([np.array(pi) for pi in parameter]), axis=0)

            rmse = []
            correlation = []

            for index_j in range(5):
                idx_start_val = index_j * 1600 + 500
                idx_end_val = idx_start_val + 400
                problem_use_val = problem_validation.iloc[idx_start_val:idx_end_val]
                y_real = list(problem_use_val[y_name].iloc[:-1])
                y0 = [problem_use_val[y_name].iloc[0]]
                u = problem_use_val[u_name].to_numpy().T
                sol = solve_ivp(fun_eval, [0, len(problem_use_val) - 1], y0, args=(u, p_param, myfun), t_eval=range(0, len(problem_use_val) - 1))

                this_rmse = round(mean_squared_error(y_real, sol.y[0], squared=False), 5)
                this_correlation = round(np.corrcoef(y_real, sol.y[0])[0][1], 5)

                rmse.append(this_rmse)
                correlation.append(this_correlation)

                plt.figure(figsize=[15, 10])
                plt.plot(y_real, 'bo', label='Data')
                plt.plot(sol.y[0], 'g-', label='Regression solve_ivp')
                plt.legend()
                plt.title(combination)
                plt.figtext(0.7, 0.2, f'RMSE: {this_rmse} ', wrap=True, horizontalalignment='left', fontsize=12)
                plt.figtext(0.7, 0.17, f'Correlation: {this_correlation} ', wrap=True, horizontalalignment='left',
                            fontsize=12)
                plt.figtext(0.7, 0.14,
                            f'Parameter: {", ".join([f"p{i}={round(pi, 5)}" for i, pi in enumerate(p_param)])}',
                            wrap=True, horizontalalignment='left', fontsize=12)
                plt.tight_layout()
                plt.savefig(f'../docs/plots/gekko/validation/{ti}_{i_combination}_{index_j}.png')
                plt.close()

            print('VALIDATION:')
            print('RMSE: ', round(np.mean(rmse), 5), '; ', round(np.median(rmse), 5))
            print('Correlation', round(np.mean(correlation), 5), '; ', round(np.median(correlation), 5))
            print()

            with open(f'../docs/results_gekko.csv', 'a') as f:
                f.write(f'{ti}_{i_combination};{round(np.mean(rmse), 5)};'
                        f'{round(np.mean(correlation), 5)};test;{combination[0]}\n')
