from lmfit import Minimizer, Parameters, report_fit

import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
import itertools
import re
import numpy as np
from scipy.integrate import solve_ivp


def replacenth(string, sub, wanted, n):
    where = [m.start() for m in re.finditer(sub, string)][n]
    before = string[:where]
    after = string[where:]
    after = after.replace(sub, wanted, 1)
    return before + after


def run(functions, problem_train, problem_validation, problem_test, x_name, y_name, u_name, ti):
    with open(f'../docs/results_lmfit.csv', 'w') as f:
        f.write(f'id;rmse;corr;train/test;fun\n')


    possible_combinations = list(itertools.product(*[functions]))

    for i_combination, combination in enumerate(possible_combinations):
        print(f'{i_combination} - used function: {combination[0]}')
        combination = list(combination)

        rmse = []
        correlation = []
        parameter = []

        def fun_eval(t, y0, u, p, fun):
            return eval(fun)

        def fcn2min(p, x, y, u, fun):
            model = eval(fun)
            return model - y

        for i_training_segment in range(10):
            start_train = i_training_segment * 3200 + 1000
            end_train = start_train + 400
            # print(f'training segments: {start_train}-{end_train}')
            problem_use = problem_train.iloc[start_train:end_train]

            xm = problem_use[x_name].to_numpy()[:-1]
            ym = np.diff(problem_use[y_name].to_numpy())
            um = problem_use[u_name].to_numpy()[:-1].T

            y_real = problem_use[y_name].iloc[:-2]

            fun = combination[0]
            p = Parameters()
            [p.add(f's{i}', value=1) for i in range(fun.count('p'))]

            for px in range(fun.count('p')):
                fun = replacenth(fun, 'p', f"p['s{px}']", px)

            fun = fun.replace('x0', 'x')

            try:
                minner = Minimizer(fcn2min, p, fcn_args=(xm, ym, um, fun))
                result = minner.minimize()

                for px in range(11):
                    fun = fun.replace(f'u[{px}]', f'u[{px}][int(t)]')

                def fun_eval(t, y0, u, p, fun):
                    return eval(fun)

                p = result.params
                fun = fun.replace('x', 'y0')

                sol = solve_ivp(fun_eval, [0, result.ndata-1], [problem_use[y_name].iloc[0]], args=(um, p, fun), t_eval=range(0, result.ndata-1))

                this_rmse = round(mean_squared_error(y_real, sol.y[0], squared=False), 5)
                this_correlation = round(np.corrcoef(y_real, sol.y[0])[0][1], 5)

                rmse.append(this_rmse)
                correlation.append(this_correlation)

                parameter.append([round(p[pi].value, 5) for pi in p])

                plt.figure(figsize=[15, 10])
                plt.plot(list(y_real), 'bo', label='Data')
                plt.plot(sol.y[0], 'g-', label='Regression solve_ivp')
                plt.legend()
                plt.title(combination)
                plt.figtext(0.7, 0.2, f'RMSE: {this_rmse} ', wrap=True, horizontalalignment='left', fontsize=12)
                plt.figtext(0.7, 0.17, f'Correlation: {this_correlation} ', wrap=True, horizontalalignment='left',
                            fontsize=12)
                plt.figtext(0.7, 0.14, f'Parameter: {", ".join([f"p{i}={round(p[pi].value, 5)}" for i, pi in enumerate(p)])}',
                            wrap=True, horizontalalignment='left', fontsize=12)
                plt.tight_layout()
                plt.savefig(f'../docs/plots/lmfit/{ti}_{i_combination}_{i_training_segment}.png')
                plt.close()

            except:
                print('FAILED!')
                print()

        if len(parameter) > 0:
            print('RMSE: ', round(np.mean(rmse), 5), '; ', round(np.median(rmse), 5))
            print('Correlation', round(np.mean(correlation), 5), '; ', round(np.median(correlation), 5))
            print('Parameter', parameter)

            with open(f'../docs/results_lmfit.csv', 'a') as f:
                f.write(f'{ti}_{i_combination};{round(np.mean(rmse), 5)};'
                        f'{round(np.mean(correlation), 5)};training;{combination[0]}\n')

            p_param = np.mean(np.array([np.array(pi) for pi in parameter]), axis=0)

            rmse = []
            correlation = []

            for si in range(fun.count('s')):
                fun = fun.replace(f"'s{si}'", str(si))

            for index_j in range(5):
                idx_start_val = index_j * 1600 + 500
                idx_end_val = idx_start_val + 400
                problem_use_val = problem_validation.iloc[idx_start_val:idx_end_val]
                y_real = list(problem_use_val[y_name].iloc[:-1])
                y0 = [problem_use_val[y_name].iloc[0]]
                u = problem_use_val[u_name].to_numpy().T
                try:
                    sol = solve_ivp(fun_eval, [0, len(problem_use_val) - 1], y0, args=(u, p_param, fun), t_eval=range(0, len(problem_use_val) - 1))
                    this_rmse = round(mean_squared_error(y_real, sol.y[0], squared=False), 5)
                    this_correlation = round(np.corrcoef(y_real, sol.y[0])[0][1], 5)

                    rmse.append(this_rmse)
                    correlation.append(this_correlation)

                    plt.figure(figsize=[15, 10])
                    plt.plot(y_real, 'bo', label='Data')
                    plt.plot(sol.y[0], 'g-', label='Regression solve_ivp')
                    plt.legend()
                    plt.title(combination)
                    plt.figtext(0.7, 0.2, f'RMSE: {this_rmse} ', wrap=True, horizontalalignment='left', fontsize=12)
                    plt.figtext(0.7, 0.17, f'Correlation: {this_correlation} ', wrap=True, horizontalalignment='left',
                                fontsize=12)
                    plt.figtext(0.7, 0.14,
                                f'Parameter: {", ".join([f"p{i}={round(pi, 5)}" for i, pi in enumerate(p_param)])}',
                                wrap=True, horizontalalignment='left', fontsize=12)
                    plt.tight_layout()
                    plt.savefig(f'../docs/plots/lmfit/validation/{ti}_{i_combination}_{index_j}.png')
                    plt.close()
                except:
                    print('Validation Error')

            if len(correlation) > 0:
                print('VALIDATION:')
                print('RMSE: ', round(np.mean(rmse), 5), '; ', round(np.median(rmse), 5))
                print('Correlation', round(np.mean(correlation), 5), '; ', round(np.median(correlation), 5))
                print()

                with open(f'../docs/results_lmfit.csv', 'a') as f:
                    f.write(f'{ti}_{i_combination};{round(np.mean(rmse), 5)};'
                            f'{round(np.mean(correlation), 5)};test;{combination[0]}\n')
