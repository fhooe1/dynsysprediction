import matplotlib.pyplot as plt
import pandas as pd
from gekko import GEKKO
from sklearn.metrics import mean_squared_error
import itertools
import re
import numpy as np


def replacenth(string, sub, wanted, n):
    where = [m.start() for m in re.finditer(sub, string)][n]
    before = string[:where]
    after = string[where:]
    after = after.replace(sub, wanted, 1)
    return before + after


def run_gekko(functions, problem, x, y, u, ti):
    problem_train = problem.iloc[:32000]
    problem_validation = problem.iloc[32000:]

    problem = problem.iloc[:400]
    plt.plot(problem.drop('index', axis=1))
    plt.show()
    xm = problem[x].to_numpy()
    ym = problem[y].to_numpy()
    um = problem[u].to_numpy()

    nvars = 1

    combinations = list(itertools.product(*[functions] * nvars))

    for ixx, c in enumerate(combinations):
        print(c)
        c = list(c)
        m = GEKKO(remote=False)
        # m.options.SOLVER = 0

        x0 = m.Param(value=xm)

        pi = 0
        for ix, ci in enumerate(c):
            for px in range(ci.count('p')):
                ci = replacenth(ci, 'p', f'p[{pi}]', px)
                pi += 1
            c[ix] = ci

        p = [m.FV() for _ in range(pi)]
        for px in p: px.STATUS = 1

        u = [m.Param(value=um[:, ui]) for ui in range(um.shape[1])]
        # for ui in u: ui.STATUS = 1

        # y = [m.CV(value=ym[i], fixed_initial=True) for i in range(nvars)]
        y = [m.CV(value=ym, fixed_initial=True)]
        for yi in y: yi.FSTATUS = 1

        # y0 = m.CV(value=ym[:, 0], fixed_initial=True)
        # y0.FSTATUS = 1

        try:
            for n in range(nvars):
                m.Equation(y[n] == eval(c[n]))

            # m.Equation(y0 == eval(c[0]))

            # m.options.IMODE = 4
            m.options.IMODE = 2

            # m.options.COLDSTART = 2
            m.solve(disp=False)

            # m.options.TIME_SHIFT = 0
            # m.options.COLDSTART = 0
            # m.solve(disp=False)

            print(f'Error:')
            for d in range(nvars):
                print(f'\tRMSE: x{d}={round(mean_squared_error(ym, y[d], squared=False), 2)}')
                print(f'\tCorrelation: x{d}={round(np.corrcoef(ym, y[d])[0][1], 2)}')

            print('Parameter: ')
            print('\t' + ', '.join([f'p{i}: {round(pi.VALUE[0], 5)}' for i, pi in enumerate(p)]))
            print()

            plt.figure(figsize=[15, 10])
            plt.plot(xm, ym, 'bo', label='Data')
            plt.plot(x0.value, np.array(y).T, 'r-', label='Regression')
            plt.legend()
            plt.title(c)
            plt.figtext(0.7, 0.2, f'RMSE: {round(mean_squared_error(ym, y[0], squared=False), 2)} ', wrap=True,
                        horizontalalignment='left', fontsize=12)
            plt.figtext(0.7, 0.17, f'Correlation: {round(np.corrcoef(ym, y[d])[0][1], 2)} ', wrap=True,
                        horizontalalignment='left', fontsize=12)
            plt.figtext(0.7, 0.14, f'Parameter: {", ".join([f"p{i}={round(pi.VALUE[0], 2)}" for i, pi in enumerate(p)])}',
                        wrap=True, horizontalalignment='left', fontsize=12)
            plt.tight_layout()
            plt.savefig(f'../docs/plots/gekko/{ti}_{ixx}.png')
            plt.close()

            with open(f'../docs/results_gekko.csv', 'a') as f:
                f.write(f'{ti}_{ixx};{round(mean_squared_error(ym, y[0], squared=False), 2)};{round(np.corrcoef(ym, y[d])[0][1], 2)}\n')


        except:
            print('FAILED!')
            print()

        m.cleanup()
