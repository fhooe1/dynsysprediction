import pandas as pd
from scipy.io import loadmat


def load_problem_data(train_split):
    mat = loadmat('../data/problems/forward_identification_without_raw_data.mat')
    # u_train = pd.DataFrame(mat['u_train'].T, columns=[f'c{i}_{v}' for i in range(6) for v in ['pos', 'vel', 'acc']])
    # u_test = pd.DataFrame(mat['u_test'].T, columns=[f'c{i}_{v}' for i in range(6) for v in ['pos', 'vel', 'acc']])
    u_train = pd.DataFrame(mat['u_train'].T, columns=[f'c{i}_torque' for i in range(6)])
    u_test = pd.DataFrame(mat['u_test'].T, columns=[f'c{i}_torque' for i in range(6)])
    y_train = pd.DataFrame(mat['y_train'].T, columns=[f'c{i}_y' for i in range(6)])
    y_test = pd.DataFrame(mat['y_test'].T, columns=[f'c{i}_y' for i in range(6)])
    train = pd.concat([u_train, y_train], axis=1)
    validation = train.iloc[int(len(train)*train_split):]
    train = train.iloc[:int(len(train)*train_split)]
    test = pd.concat([u_test, y_test], axis=1)
    return train.reset_index(), validation.reset_index(), test.reset_index()

