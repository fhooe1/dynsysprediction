import pandas as pd


def load_problem_data():
    df = pd.read_csv('../data/problems/exp.csv', sep=";", decimal=",")
    return df