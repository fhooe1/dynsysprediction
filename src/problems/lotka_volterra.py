import pandas as pd


def load_problem_data():
    df = pd.read_csv('../data/problems/lotka_volterra_2.csv', sep=";", decimal=",")
    return df.iloc[:300]