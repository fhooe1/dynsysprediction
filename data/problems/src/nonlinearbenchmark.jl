using DifferentialEquations

# Generate original data
function f(du,u,p,t)
    du[1] = 1.01*u[1]
end

u0 = [1/2]
tspan = (1.0, 5.0)
tsteps = tspan[1]:0.1:tspan[2]
prob = ODEProblem(f, u0, tspan)
sol_real = solve(prob,Tsit5(),saveat=tsteps)




# Load functions






# Loop over functions and DiffEqFlux









using ExprRules

# Create grammar
const grammar = @grammar begin
    Real = u[1]
    Real = Real * Real
    Real = Real + Real
    Real = Real - Real
    # Real = Real / Real
    Real = p
end

const S = SymbolTable(grammar)
dmap = mindepth_map(grammar)

tr = rand(RuleNode, grammar, :Real, dmap, 4)
exp = get_executable(tr, grammar)

# Convert function and create params
strexp = ""
ci = 1
p = Vector{Float64}()
for c in (string(exp))
    global strexp
    global ci
    if c == 'p'
        strexp = strexp * "p[" * string(ci) * "]"
        push!(p, Base.randn())
        ci = ci + 1
    else
        strexp = strexp * c
    end
end

print(tr)
print("\n")
print(strexp)
print("\n")

exp = Meta.parse(strexp)

freal = @eval function(du, u, p, t)
    du[1] = $exp
end

p = [Base.randn() for i in 1:count(i->(i=='p'), strexp)]


# Optimize data
using DiffEqFlux
using StatsBase
using Plots

prob = ODEProblem(freal, u0, tspan, p)

function loss(p)
    sol = solve(prob, Tsit5(), p=p, saveat = tsteps)
    if size(sol.u)[1] == size(sol_real.u)[1]
        loss = StatsBase.meanad(reduce(vcat, sol_real.u), reduce(vcat, sol.u))
    else
        loss = Inf
    end
    return loss, sol
end

callback = function (p, l, pred)
    print(l)
    plt = plot(pred, ylim = (0, 6)) 
    print("\n")
    display(plt)
    # Tell sciml_train to not halt the optimization. If return true, then
    # optimization stops.
    return false
end

result_ode = DiffEqFlux.sciml_train(loss, p, cb = callback, maxiters=100)
print("HALLO")
print(result_ode)
# result_ode = DiffEqFlux.sciml_train(loss, p, ADAM(0.1), cb = callback, maxiters=1000)

# Use optimal parameters
prob = ODEProblem(freal,  u0, tspan, result_ode.u)
sol_fun = solve(prob)

using Plots
plot(sol_fun, linewidth=5, label="GP Solution") # legend=false
plot!(sol_real, linewidth=3,ls=:dash,label="True Solution!")