

#  load different problem data


#  define training/validation/test


#  define algorithm

import random
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import numpy as np

possible_solutions = ['00', '0A', '0B', 'AA', 'BB', 'AB']

population_size = 1000
generations = 1000

population = np.array([random.choice(possible_solutions) for _ in range(population_size)])
# population = ['00'] * 1000 + ['0A'] * 500 + ['AA'] * 500 + ['0B'] * 500 + ['BB'] * 500 + ['AB'] * 1000


pop_sizes = pd.DataFrame(columns=possible_solutions)

for g in range(generations):
    print(f'generation: {g}')
    new_population = np.empty(population_size, dtype='<U2')
    for px in range(population_size):
        p1 = random.choice(population)
        p2 = random.choice(population)

        p1_allele = random.choice(p1)
        p2_allele = random.choice(p2)

        child = ''.join(sorted([p1_allele, p2_allele]))
        new_population[px] = child

    unique, counts = np.unique(new_population, return_counts=True)
    pop_sizes.loc[len(pop_sizes)] = dict(zip(unique, counts))
    population = new_population

plt.plot(pop_sizes['00'], label='0')
plt.plot([ai + aj for ai, aj in zip(pop_sizes['0A'], pop_sizes['AA'])], label='A')
plt.plot([bi + bj for bi, bj in zip(pop_sizes['0B'], pop_sizes['BB'])], label='B')
plt.plot(pop_sizes['AB'], label='AB')
plt.legend()
plt.show()

plt.plot(pop_sizes['00'], label='00')
plt.plot(pop_sizes['0A'], label='0A')
plt.plot(pop_sizes['0B'], label='0B')
plt.plot(pop_sizes['AA'], label='AA')
plt.plot(pop_sizes['BB'], label='BB')
plt.plot(pop_sizes['AB'], label='AB')
plt.legend()
plt.show()

df = pd.DataFrame(pop_sizes)

corr = df.iloc[1:].corr()
sns.heatmap(corr,
            xticklabels=corr.columns.values,
            yticklabels=corr.columns.values,
            vmin=-1, vmax=1, annot=True)
plt.show()

