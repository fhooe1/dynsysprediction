import numpy as np
import matplotlib.pyplot as plt
import sklearn.gaussian_process as gp
from sklearn.neural_network import MLPRegressor
from sklearn import svm
from sklearn.metrics import r2_score
# import tensorflow as tf
from gekko.ML import Gekko_GPR, Gekko_SVR, Gekko_NN_SKlearn
from gekko.ML import Gekko_NN_TF, Gekko_LinearRegression
from gekko.ML import Bootstrap, Conformist, CustomMinMaxGekkoScaler
import pandas as pd
from sklearn.model_selection import train_test_split
from gekko import GEKKO


# Source function to generate data
def f(x):
    return np.cos(2 * np.pi * x)


# represent noise from a data sample
N = 350
xl = np.linspace(0, 1.2, N)
noise = np.random.normal(0, .3, N)
y_measured = f(xl) + noise

plt.plot(xl, f(xl), label='source function')
plt.plot(xl, y_measured, '.', label='measured points')
plt.legend()
plt.show()

data = pd.DataFrame(np.array([xl, y_measured]).T, columns=['x', 'y'])
features = ['x']
label = ['y']

train, test = train_test_split(data, test_size=0.2, shuffle=True)

k = gp.kernels.RBF() * gp.kernels.ConstantKernel() + gp.kernels.WhiteKernel()
gpr = gp.GaussianProcessRegressor(kernel=k,
                                  n_restarts_optimizer=10,
                                  alpha=0.1,
                                  normalize_y=True)
gpr.fit(train[features], train[label])
r2 = gpr.score(test[features], test[label])
print('gpr r2:', r2)
print(gpr.get_params())
print(gpr.kernel_)

svr = svm.SVR()
svr.fit(train[features], np.ravel(train[label]))
r2 = svr.score(test[features], np.ravel(test[label]))
print('svr r2:', r2)

plt.figure(figsize=(8, 8))
plt.plot(xl, f(xl), label='source function')
plt.plot(xl, y_measured, '.', label='measured points')
gpr_pred, gpr_u = gpr.predict(data[features], return_std=True)
gpr_u *= 1.645
# plt.errorbar(xl, gpr_pred, fmt='--', yerr=gpr_u, label='gpr')
plt.plot(xl, gpr_pred, '--', label='gpr')
plt.plot(xl, svr.predict(data[features]), '--', label='svr')
plt.legend()
plt.show()

m = GEKKO()
x = m.Var(0, lb=0, ub=1)
y = Gekko_GPR(gpr, m).predict(x)  # function is used here
m.Obj(y)
m.solve(disp=False)
print('solution:', y.value[0])
print('x:', x.value[0])
print('Gekko Solvetime:', m.options.SOLVETIME, 's')
