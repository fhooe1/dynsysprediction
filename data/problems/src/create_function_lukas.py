import re
from sympy import symbols, Symbol
from sympy.parsing.sympy_parser import parse_expr
import matplotlib.pyplot as plt
import time


def replacenth(string, sub, wanted, n):
    where = [m.start() for m in re.finditer(sub, string)][n]
    before = string[:where]
    after = string[where:]
    after = after.replace(sub, wanted, 1)
    return before + after


grammar = {
    'Expr': ['Term + Expr', 'Term + p', 'Term - Expr', 'Term - p'],
    'Term': ['Factor * Term', 'Factor * p'],
    'Factor': ['var', 'NLF'],  # Nonlinear Factor
    'NLF': [],  # Nonlinear Function
    'FAE': ['FAT + FAE', 'FAT + p'],  # FuncArgExpr
    'FAT': ['var * FAE', 'var + p'],  # FuncArgTerm
    'var': ['x1', 'x2']
}

x1 = symbols(' '.join(grammar['var']))
p = Symbol('p')

increase_complexity_operator = ['log', 'exp', '+', '*']
increase_complexity_variables = grammar['var'] + list(grammar.keys())
max_complexity = 3

variable_renaming = {
    'x1': 'x1'
}


def calculate_complexity(element, increase_complexity):
    return sum([element.count(op) for op in increase_complexity])


def calculate_grammar(complexity):
    start = time.time()
    queue = ['Expr']
    final_queue = []
    queue_evaluated = []

    length = []

    while queue:
        length.append([len(queue), len(final_queue)])
        print(length[-1])
        element = queue.pop()
        if element in queue_evaluated:
            continue
        queue_evaluated.append(element)
        for k in grammar.keys():
            for f in range(element.count(k)):
                for o in grammar[k]:
                    new_element = replacenth(element, k, o, f)
                    if (not any(ext in new_element for ext in grammar.keys())) and \
                            (calculate_complexity(new_element, increase_complexity_variables) <= complexity):
                        new_element_parsed = Symbol(re.sub('(?<!x)\d\*', '', str(parse_expr(new_element))))
                        if new_element_parsed not in final_queue:
                            final_queue.append(new_element_parsed)
                        continue
                    if calculate_complexity(new_element, increase_complexity_variables) <= complexity:
                        queue.append(new_element)
                        continue
    duration = time.time() - start
    # for f in final_queue:
    #     print(f)
    print(f'complexity {complexity}: length {len(final_queue)}, duration: {round(duration, 2)}')
    plt.plot(length, label=['len(queue)', 'len(final_queue)'])
    plt.title(f'complexity: {complexity}')
    plt.legend()
    plt.show()
    return final_queue


final_queue = calculate_grammar(max_complexity)

with open(f'functions_complexity_{max_complexity}.txt', 'w') as f:
    for e in final_queue:
        je = str(e)
        rep = dict((re.escape(k), v) for k, v in variable_renaming.items())
        pattern = re.compile("|".join(rep.keys()))
        je = pattern.sub(lambda m: rep[re.escape(m.group(0))], je)
        f.write(f"{str(je)}\n")
print(len(final_queue))

