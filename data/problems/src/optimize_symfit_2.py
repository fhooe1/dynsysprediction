from symfit import variables, Parameter, Fit, D, ODEModel, exp
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import re

with open('functions_complexity_2.txt', 'r') as f:
    functions = f.read().splitlines()

df = pd.read_csv('lotka_volterra_2.csv', sep=";", decimal=',')
tx = df['t'].to_numpy()
ym = df[['predator', 'prey']].to_numpy()

for dx1 in functions:
    for dx2 in functions:
        pi = 0
        np = 0
        np += len(re.findall('(?<!x)p', dx1))
        for i in range(pi, np):
            dx1 = re.sub('(?<!x)p(?!\d)', f'p{i}', dx1, count=1)
        pi += np
        np += len(re.findall('(?<!x)p', dx2))
        for i in range(pi, np):
            dx2 = re.sub('(?<!x)p(?!\d)', f'p{i}', dx2, count=1)
        fun = f'dx1/dt = {dx1}\ndx2/dt = {dx2}'
        print(fun)

        x1, x2, t = variables('x1, x2, t')
        p0 = Parameter('p0', 0.1, max=10)
        p1 = Parameter('p1', 0.1, max=10)
        p2 = Parameter('p2', 0.1, max=10)
        p3 = Parameter('p3', 0.1, max=10)
        p4 = Parameter('p4', 0.1, max=10)
        p5 = Parameter('p5', 0.1, max=10)
        p6 = Parameter('p6', 0.1, max=10)
        p7 = Parameter('p7', 0.1, max=10)

        x1_0 = ym[0, 0]
        x2_0 = ym[0, 1]

        model_dict = {
            D(x1, t): p0*x1 - p1*x1*x2,
            D(x2, t): p2*x1 - p3*x1*x2
        }

        ode_model = ODEModel(model_dict, initial={t: 0.0, x1: x1_0, x2: x2_0})

        try:
            fit = Fit(ode_model, t=tx, x1=ym[:, 0], x2=ym[:, 1])
            fit_result = fit.execute()

            t = df['t'].to_numpy()
            tvec = np.linspace(t[0], t[-1], len(t))

            X = ode_model(t=tvec, **fit_result.params)
            plt.plot(tvec, X.output[0], c='r', label='predicted')
            plt.scatter(t, ym, label='original')
            plt.legend()
            plt.title(fun)
            plt.show()
        except:
            print('ERROR!')